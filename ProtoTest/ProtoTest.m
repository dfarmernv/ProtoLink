(* Wolfram Language Package *)

(* Created by the Wolfram Workbench Aug 3, 2018 *)

BeginPackage["ProtoTest`", {"JLink`"}]
(* Exported symbols added here with SymbolName::usage *) 
myFun::usage = "Foo"
BuildReader::usage = "Create proto reader given descriptor set file path"
Begin["`Private`"]
(* Implementation of the package *)

myFun[i_, protofile_, mtype_, msgpath_] := 
	Module[{obj}, 
		obj = JavaNew["DynamicProtobufReader", i];
		obj@ReadProto[protofile, mtype, msgpath]
	]
	
BuildReader[descriptorSet_] := Module[{obj},
	obj = JavaNew["DynamicProtobufReader", descriptorSet];
	Function[{protofile, msgtype, msgpath}, obj@ReadProto[protofile, msgtype, msgpath]]	
]

End[]

EndPackage[]

