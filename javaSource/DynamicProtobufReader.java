import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.DescriptorProtos.FileDescriptorProto;
import com.google.protobuf.DescriptorProtos.FileDescriptorSet;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Descriptors.DescriptorValidationException;
import com.google.protobuf.Descriptors.EnumValueDescriptor;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.Descriptors.FileDescriptor;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.Enum;
import com.wolfram.jlink.KernelLink;

import com.wolfram.jlink.MathLinkException;

import com.wolfram.jlink.StdLink;

@SuppressWarnings("unchecked")
public class DynamicProtobufReader {
	private FileDescriptorSet fds;
	KernelLink link = StdLink.getLink();

	public DynamicProtobufReader(String path) throws IOException {
		FileInputStream input = new FileInputStream(path);
		fds = DescriptorProtos.FileDescriptorSet.parseFrom((InputStream) input);
	}

	public DynamicMessage ReadProto(String protofile, String messageTypeName, String dataFile)
			throws IOException, DescriptorValidationException, MathLinkException {
		Optional<FileDescriptorProto> match = fds.getFileList().stream()
				.filter(i -> i.getName().toLowerCase().equals(protofile.toLowerCase())).findFirst();
		// FIXME: Error handling for NoSuchElement
		FileDescriptorProto fdp = match.get();
		FileDescriptor[] deps = new FileDescriptor[0];
		FileDescriptor fd = FileDescriptor.buildFrom(fdp, deps);
		Optional<Descriptor> messageDesc = fd.getMessageTypes().stream()
				.filter(s -> s.getName().toLowerCase().contains(messageTypeName.toLowerCase())).findFirst();
		FileInputStream data = new FileInputStream(dataFile);
		DynamicMessage output = DynamicMessage.parseFrom(messageDesc.get(), data);
		Descriptor messageType = messageDesc.get();

		link.beginManual();
		link.putFunction("Association", messageType.getFields().size());
		Deserialize(messageType, output);
		return output;
	}

	private void Deserialize(Descriptor messageType, DynamicMessage output) throws MathLinkException {
		Iterator<FieldDescriptor> iter = messageType.getFields().iterator();
		link.putFunction("Association", messageType.getFields().size());
		while (iter.hasNext()) {
			FieldDescriptor field = iter.next();
			link.putFunction("Rule", 2);
			link.put(field.getName());
			Object obj = output.getField(field);
			if (obj instanceof Collection<?>) {
				link.putFunction("List", ((Collection<Object>) obj).size());
				Iterator<Object> inneriter = ((Collection<Object>) obj).iterator();
				while (inneriter.hasNext()) {
					Object temp = inneriter.next();
					if (temp instanceof DynamicMessage) {
						Deserialize(field.getMessageType(), (DynamicMessage) temp);
					}
				}

			} else if (field.getJavaType() == Descriptors.FieldDescriptor.JavaType.ENUM) {
				link.put(((EnumValueDescriptor) obj).toString());
			} else {
				link.put(output.getField(field));
			}
		}
	}
}
